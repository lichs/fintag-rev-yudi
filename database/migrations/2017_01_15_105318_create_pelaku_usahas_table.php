<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelakuUsahasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('pelaku_usaha', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('user_id')->unsigned();
          $table->string('nik');
          $table->string('nama')->nullable();
          $table->string('no_telp')->nullable();
          $table->string('jenis_kelamin')->nullable();
          $table->string('nama_ibu_kandung')->nullable();
          $table->string('kota')->nullable();
          $table->string('kelurahan')->nullable();
          $table->string('kecamatan')->nullable();
          $table->string('alamat')->nullable();
          $table->integer('kode_pos')->nullable();
          $table->string('pendidikan_terakhir')->nullable();
          $table->string('status')->nullable();
          $table->string('password_temporary')->nullable();
          $table->timestamps();

          $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelaku_usaha');
    }
}
