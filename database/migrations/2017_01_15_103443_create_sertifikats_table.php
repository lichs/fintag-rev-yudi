<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSertifikatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sertifikat', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('agent_id')->unsigned();
          $table->string('no_sertifikat');
          $table->date('tanggal_awal');
          $table->date('tanggal_akhir');
          $table->string('sertifikat_image');
          $table->timestamps();
      });

      Schema::table('sertifikat', function($table){
          $table->foreign('agent_id')->references('id')->on('agents');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sertifikat');
    }
}
