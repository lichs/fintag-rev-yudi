<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('request_agent', function(Blueprint $table){
          $table->increments('id');
          $table->string('ticket_no');
          $table->integer('pelaku_usaha_id')->unsigned();
          $table->integer('agent_id')->unsigned();
          $table->string('trigger_by');
          $table->smallInteger('accepted')->default(0);
          $table->timestamps();

          $table->foreign('agent_id')->references('id')->on('agents');
          $table->foreign('pelaku_usaha_id')->references('id')->on('pelaku_usaha');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_agent');
    }
}
