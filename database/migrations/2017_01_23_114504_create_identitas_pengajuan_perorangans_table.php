<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIdentitasPengajuanPerorangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identitas_pengajuan_perorangan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pengajuan_id')->unsigned();
            $table->string('ktp_image')->nullable();
            $table->string('npwp')->nullable();
            $table->string('npwp_image')->nullable();
            $table->string('status_npwp')->nullable();
            $table->string('status_pernikahan')->nullable();
            $table->string('alamat_ktp')->nullable();
            $table->string('alamat')->nullable();
            $table->string('status_kepemilikan_rumah')->nullable();
            $table->string('lama_tahun_menempati_rumah')->nullable();
            $table->string('lama_bulan_menempati_rumah')->nullable();
            $table->string('status_kartu_keluarga')->nullable();
            $table->string('kartu_keluarga_image')->nullable();
            $table->string('status_akta_nikah_cerai')->nullable();
            $table->string('akta_nikah_cerai_image')->nullable();
            $table->string('status_ktp_pasangan')->nullable();
            $table->string('ktp_pasangan_image')->nullable();
            $table->string('status_siup')->nullable();
            $table->string('siup_image')->nullable();
            $table->string('status_tdp')->nullable();
            $table->string('tdp_image')->nullable();
            $table->string('status_skdp_situ')->nullable();
            $table->string('skdp_situ_image')->nullable();
            $table->string('status_akte_pendirian')->nullable();
            $table->string('akte_pendirian_image')->nullable();
            $table->string('status_ijin_usaha')->nullable();
            $table->string('ijin_usaha_image')->nullable();
            $table->string('status_kontrak')->nullable();
            $table->string('kontrak_image')->nullable();
            $table->string('status_nasabah_bank')->nullable();
            $table->string('rek_tabungan_image')->nullable();
            $table->string('status_laporan_keungan')->nullable();
            $table->string('laporan_keuangan_image')->nullable();
            $table->string('status_cash_flow')->nullable();
            $table->string('cash_flow_image')->nullable();
            $table->string('status_daftar_hutang')->nullable();
            $table->string('daftar_hutang_image')->nullable();
            $table->string('status_daftar_mesin')->nullable();
            $table->string('daftar_alat_image')->nullable();
            $table->string('berat_mesin')->nullable();
            $table->string('status_daftar_penjamin')->nullable();
            $table->string('daftar_penjamin_image')->nullable();
            $table->string('status_daftar_keuangan_penjamin')->nullable();
            $table->string('daftar_keuangan_penjamin_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identitas_pengajuan_perorangan');
    }
}
