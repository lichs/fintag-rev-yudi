<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRekapKeuanganUsahasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rekap_keuangan_usaha', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('identitas_pengajuan_perorangan_id')->unsigned();
            $table->float('nilai_penjualan', 10, 2)->nullable();
            $table->float('biaya_bagi_hasil', 10, 2)->nullable();
            $table->float('biaya_tidak_tetap', 10, 2)->nullable();
            $table->float('biaya_tetap', 10, 2)->nullable();
            $table->float('laba_bersih', 10, 2)->nullable();
            $table->timestamps();

            $table->foreign('identitas_pengajuan_perorangan_id')->references('id')->on('identitas_pengajuan_perorangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekap_keuangan_usaha');
    }
}
