<?php

namespace App\Http\ViewComposer;

use Auth;
use App\User;
use Illuminate\View\View;

class ProfileComposer
{

	public function compose(View $view)
	{
		if (! Auth::check()) {
			return ;
		}

		$agent = User::find(Auth::user()->id)->agent;

		$view->with(['agents' => $agent]);
	}

}
