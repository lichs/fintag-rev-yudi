<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;

class RoleRedirectMiddleware
{
  public function handle($request, Closure $next, $roleName)
  {
    if (auth()->check() && ! auth()->user()->hasRole($roleName))
    {
      return abort(401, 'Unauthorize');
    }

    return $next($request);
  }
}
