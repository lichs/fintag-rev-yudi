<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

      view()->composer('agent.dashboard', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.permintaanPengajuan', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.data_pengajuan', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.dompet', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.point', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.pengajuan_badan_usaha', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.pengajuan_perorangan', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile_badan_usaha', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile_usaha', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile_keuangan', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile_pembiayaan', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.profile_pribadi', \App\Http\ViewComposer\ProfileComposer::class);

      view()->composer('agent.page.persyaratan_dokumen', \App\Http\ViewComposer\ProfileComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
