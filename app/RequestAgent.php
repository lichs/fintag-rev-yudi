<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RequestAgent extends Model
{
    protected $table = 'request_agent';

    protected $dates = ['created_at'];

    protected $fillable = ['ticket_no', 'pelaku_usaha_id', 'agent_id', 'trigger_by', 'accepted'];

    public function pengajuanPu()
    {
        return $this->hasMany(PengajuanPu::class, 'request_id', 'id');
    }

    public function agent()
    {
    	return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function pelakuUsaha()
    {
        return $this->belongsTo(PelakuUsaha::class);
    }

    public function is_accepted()
    {
    	return (bool) $this->accepted;
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }
}
