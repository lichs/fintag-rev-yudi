@extends('layouts.app')

@section('content')

	<div id="container" class="effect aside-float aside-bright mainnav-lg">

		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
						<div id="page-content">
							<div class="panel">
								<div class="panel-body pad-no">
									<h3 class="panel-title">Data Pengajuan</h3>
									<ol class="breadcrumb">
										<li class="home">Beranda</li>
										<li class="active">Data Pengajuan</li>
									</ol>
								</div>
								<div class="panel-body">
									<fieldset>
										<div class="form-group">
											<label class="col-sm-1 control-label mar-ver" >Dari</label>
											<div class="col-sm-3 mar-ver">
												<div class="input-group date">
													<input type="text" placeholder="Pilih Tanggal Awal"  class="form-control dtpicker">
													<span class="input-group-addon"><i class="pli-calendar-4"></i></span>
												</div>
											</div>
											<label class="col-sm-1 control-label mar-ver" >Sampai</label>
											<div class="col-sm-3 mar-ver">
												<div class="input-group date">
													<input type="text" placeholder="Pilih Tanggal Akhir"  class="form-control dtpicker">
													<span class="input-group-addon"><i class="pli-calendar-4"></i></span>
												</div>
											</div>
											<div class="col-sm-1 mar-ver">
												<button class="btn btn-default btn-icon" type="submit"><i class="fa fa-search"></i></button>
											</div>
										</div>
									</fieldset>
									<div class="clearfix mar-ver"></div>
									<table id="data-pengajuan" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Ticket no.</th>
												<th>Pelaku Usaha</th>
												<th>No Telp</th>
												<th>Tanggal Request</th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				@include('agent.sidebar')
			</div>

		@endforeach

	</div>

@endsection

@push('script')
		<script>

				$(function(){

						$('#data-pengajuan').DataTable({
								responsive: true,
								processing: true,
				        serverSide: true,
				        ajax: "/agent/data/data-pengajuan",
								columns: [
									{ data: 'ticket_no', name: 'Ticket No.' , orderable: true},
									{ data: 'pelaku_usaha.nama', name: 'Pelaku Usaha'},
									{ data: 'pelaku_usaha.no_telp', name: 'No Telp'},
									{ data: 'created_at', name: 'Tanggal Request' },
									{ data: 'action', name: 'Action', orderable: true, searchable: false},
								]
						});

				});

		</script>
@endpush
