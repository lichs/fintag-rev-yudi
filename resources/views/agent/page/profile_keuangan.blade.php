@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					  <div id="content-container">
					      <div id="page-content">
                  <div class="panel">
                    <div class="panel-body pad-no">
                        <h3 class="panel-title">Form Pengajuan Badan Usaha</h3>
                        <ol class="breadcrumb">
                          <li class="home">Beranda</li>
                          <li><a href="#">Pendataan Pengajuan</a></li>
                          <li>Form Pengajuan Badan Usaha</li>
                          <li class="active">Profil Keuangan</li>
                        </ol>
                    </div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam mauris eros, dictum id nisi convallis, efficitur ultrices mauris. In condimentum eu risus eu gravida. In hac habitasse platea dictumst. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse finibus mollis magna, at porttitor felis dictum et. Nulla sit amet est velit. Donec malesuada urna vel nisl faucibus, sed laoreet urna porta. </p>
                    </div>
                  </div>
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="panel">
                              <div class="panel-heading">
                                  <h3 class="panel-title">Profile Keuangan</h3>
                              </div>
                              <form class="panel-body form-horizontal form-padding" action="#" method="post">
                                <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Penghasilan Perbulan</span></h3>
                                  <div class="pad-hor">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" >Penghasilan Pemohon Rp. *</label>
                                            <div class="col-md-4">
                                                <input type="number" min="0"  class="form-control text-right">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" >Penghasilan Pasangan Rp. *</label>
                                            <div class="col-md-4">
                                                <input type="number" min="0"  class="form-control  text-right">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" >Penghasilan Lain Rp. *</label>
                                            <div class="col-md-4">
                                                <input type="number" min="0"  class="form-control  text-right">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-7 control-label" >Total Penghasilan Kotor Rp. *</label>
                                            <div class="col-md-4">
                                                <input type="text"  class="form-control  text-right" readonly>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Pengeluaran Perbulan</span></h3>
                                  <div class="pad-hor">
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <label class="col-md-7 control-label" >Pengeluaran Perbulan Rp. *</label>
                                            <div class="col-md-4">
                                                <input type="text"  class="form-control  text-right" >
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Keikutsertaan sebagai Anggota Koperasi</span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Pernah menjadi Anggota Koperasi *</label>
                                              <div class="col-md-9">
                                                  <div class="radio">
                                                      <input id="rbtKoperasi-1" class="magic-radio" type="radio" name="rbtKoperasi" onClick="setformAnggotaKoperasi(this.value)" value="Ya">
                                                      <label for="rbtKoperasi-1">Ya</label>
                                                      <input id="rbtKoperasi-2" class="magic-radio" type="radio" name="rbtKoperasi" onClick="setformAnggotaKoperasi(this.value)" value="Tidak"  checked>
                                                      <label for="rbtKoperasi-2">Tidak</label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-10 field-group" id="formDataAnggotaKoperasi">
                                              <h3 class="h6 mar-ver"><span>Data Pinjaman di Koperasi</span></h3>
                                              <div class="col-sm-10">
                                                  <div class="form-group">
                                                      <label class="col-md-4 control-label" >Apakah sedang ada pinjaman *</label>
                                                      <div class="col-md-6">
                                                          <div class="radio">
                                                              <input id="rbtPKoperasi-1" class="magic-radio" type="radio" name="rbtPKoperasi" onClick="setformDataKoperasi(this.value)" value="Ya">
                                                              <label for="rbtPKoperasi-1">Ya</label>
                                                              <input id="rbtPKoperasi-2" class="magic-radio" type="radio" name="rbtPKoperasi" onClick="setformDataKoperasi(this.value)" value="Tidak" checked>
                                                              <label for="rbtPKoperasi-2">Tidak</label>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-sm-12" id="formDataKoperasi">
                                                  <div class="form-group">
                                                  <label class="col-md-7 control-label" >Angsuran Perbulan Rp.*</label>
                                                  <div class="col-md-5">
                                                      <input type="number" min="0"  class="form-control text-right" >
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="mar-ver"></div>
                                          </div>
                                      </div>
                                  </div>
                                </div>

                                <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Kewajiban pada Kreditur Lain <i>(Bank dan/atau Perusahaan Pembiayaan Lainnya)</i></span></h3>
                                  <div class="pad-hor">
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-3 control-label" >Memiliki kewajiban pembayaran *</label>
                                              <div class="col-md-9">
                                                  <div class="radio">
                                                      <input id="rbtKreditur-1" class="magic-radio" type="radio" name="rbtKreditur" onClick="setformKreditur(this.value)" value="Ya" >
                                                      <label for="rbtKreditur-1">Ya</label>
                                                      <input id="rbtKreditur-2" class="magic-radio" type="radio" name="rbtKreditur"  onClick="setformKreditur(this.value)" value="Tidak" checked>
                                                      <label for="rbtKreditur-2">Tidak</label>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-sm-10 field-group" id="formDataKreditur">
                                              <h3 class="h6 mar-ver"><span>Data Pinjaman Kreditur</span></h3>
                                              <div class="col-sm-12">
                                                  <div class="form-group">
                                                      <label class="col-md-7 control-label" >Angsuran Perbulan Rp.*</label>
                                                      <div class="col-md-5">
                                                      <input type="number" min="0"  class="form-control text-right" >
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="mar-ver"></div>
                                          </div>
                                      </div>
                                  </div>
                                </div>

                                <div class="col-sm-12">
                                  <h3 class="h5 mar-ver"><span>Keikutsertaan sebagai Anggota KUB</span></h3>
                                  <div class="pad-hor">
                                    <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Pernah menjadi Anggota KUB *</label>
                                          <div class="col-md-9">
                                              <div class="radio">
                                                  <input id="rbtKUB-1" class="magic-radio" type="radio" name="rbtKUB" onClick="setformKUB(this.value)" value="Ya" >
                                                  <label for="rbtKUB-1">Ya</label>
                                                  <input id="rbtKUB-2" class="magic-radio" type="radio" name="rbtKUB" onClick="setformKUB(this.value)" value="Tidak" checked >
                                                  <label for="rbtKUB-2">Tidak</label>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10 field-group" id="formDataKUB">
                                        <h3 class="h6 mar-ver"><span>Data Pinjaman di KUB</span></h3>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Nama KUB*</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Alamat KUB*</label>
                                                <div class="col-md-8">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >No Telp KUB*</label>
                                                <div class="col-md-7">
                                                    <input type="text"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" >Apakah sedang ada pinjaman *</label>
                                                <div class="col-md-6">
                                                    <div class="radio">
                                                        <input id="rbtPKUB-1" class="magic-radio" type="radio" name="rbtPKUB" onClick="setformPinjamanKUB(this.value)" value="Ya">
                                                        <label for="rbtPKUB-1">Ya</label>
                                                        <input id="rbtPKUB-2" class="magic-radio" type="radio" name="rbtPKUB" onClick="setformPinjamanKUB(this.value)" value="Tidak" checked>
                                                        <label for="rbtPKUB-2">Tidak</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12" id="formPinjamanKUB">
                                            <div class="form-group">
                                                <label class="col-md-7 control-label" >Angsuran Perbulan Rp.*</label>
                                                <div class="col-md-5">
                                                    <input type="number" min="0"  class="form-control text-right" >
                                                </div>
                                            </div>
                                        </div>
                                      <div class="mar-ver"></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-sm-12 mar-ver pad-ver" style="border:1px dashed #ddd;background:#f4f4f4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label" >Total Penghasilan Rp.*</label>
                                        <div class="col-md-3">
                                            <input type="number" min="0"  class="form-control text-right" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12 mar-ver">
                                    <div class="col-sm-4">
                                    	<a class="btn btn-default btn-block mar-ver" href="{{ redirect()->back()->getTargetUrl() }}">Tutup Halaman ini</a>
                                    </div>
                                    <div class="col-sm-4">
                                    	  <button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
                                    </div>
                                </div>
                              </form>
                          </div>
                      </div>
                  </div>
					      </div>
					  </div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
	<script>
		$( document ).ready(function() {
			$('.dtpicker').datepicker({
				dateFormat : "dd/mm/yy"
			});
		$('.selectpicker').selectpicker('refresh');
		$('#formDataAnggotaKoperasi').hide();
		$('#formDataKreditur').hide();
		$('#formDataKoperasi').hide();
		$('#formDataKUB').hide();
		$('#formDataPinjamanKoperasi').hide();
		$('#formPinjamanKUB').hide();
		});
		function setformAnggotaKoperasi(val) {
		if (val=='Ya') {
		$('#formDataAnggotaKoperasi').show();
		}else{
		$('#formDataAnggotaKoperasi').hide();
		}
		}
		function setformKreditur(val){
		if (val=='Ya') {
		$('#formDataKreditur').show();
		}else{
		$('#formDataKreditur').hide();
		}
		}
		function setformDataKoperasi(val){
		if (val=='Ya') {
		$('#formDataKoperasi').show();
		}else{
		$('#formDataKoperasi').hide();
		}
		}
		function setformKUB(val){
		if (val=='Ya') {
		$('#formDataKUB').show();
		}else{
		$('#formDataKUB').hide();
		}
		}
		function setformPinjamanKUB(val){
		if (val=='Ya') {
		$('#formPinjamanKUB').show();
		}else{
		$('#formPinjamanKUB').hide();
		}
		}
	</script>
@endpush
