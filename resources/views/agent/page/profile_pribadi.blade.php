@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		@foreach($agents as $agent)
			<div class="boxed">
				@include('agent.navigation')
					<div id="content-container">
					  <div id="page-content">
              <div class="panel">
              	<div class="panel-body pad-no">
                    <h3 class="panel-title">Form Perorangan</h3>
                    <ol class="breadcrumb">
                      <li class="home">Beranda</li>
                      <li><a href="#">Pendataan Pengajuan</a></li>
                      <li>Form Pengajuan Badan Usaha</li>
                      <li class="active">Profil Pribadi</li>
                    </ol>
              	</div>
                <div class="panel-body">
										<div class="well bg-danger">
												<p class="text-bold">Untuk identitas pemohon tidak bisa diubah, hanya dapat menambahkan informasi tambahan seperti alamat domisili sekarang jika alamat pemohon sekarang berbeda dengan data yang tampil, no telp, NPWP, referensi kerabat tidak serumah, dan Rekening aktif. </p>
										</div>
                </div>
              </div>

							@foreach ($pelaku_usaha as $pu)
              <div class="row">
                  <div class="col-lg-12">
                      <div class="panel">
                          <div class="panel-heading">
                              <h3 class="panel-title">Profile Pribadi</h3>
                          </div>
                          <form class="panel-body form-horizontal form-padding" action="/agent/pengajuan-perorangan/profile-pribadi" method="post">
														{{ csrf_field() }}
														<input type="hidden" name="pelaku_usaha_id" value="{{ $pu->id }}">
														<input type="hidden" name="pengajuan_id" value="{{ $pengajuan_id }}">
														<input type="hidden" name="cursor" value="1">
                            <div class="col-sm-12">
                              <h3 class="h5 mar-ver"><span>Identitas Pemohon</span></h3>
                              <div class="pad-hor">
																	<div class="col-sm-10">
																			<div class="form-group">
																					<label class="col-md-3 control-label" >NIK *</label>
																					<div class="col-md-9">
																							<input type="text"  class="form-control" value="{{ $pu->nik }}" disabled>
																					</div>
																			</div>
																	</div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Nama *</label>
                                          <div class="col-md-9">
                                              <input type="text"  class="form-control" value="{{ $pu->nama }}" disabled>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Jenis Kelamin *</label>
                                          <div class="col-md-9">
                                              <div class="radio">
		                                              <input id="rbtJK" class="magic-radio" type="radio" name="rbtJK" value="Pria" checked>
		                                              <label for="rbtJK">Laki-Laki</label>
		                                              <input id="rbtJK-2" class="magic-radio" type="radio" name="rbtJK" value="Wanita" >
		                                              <label for="rbtJK-2">Perempuan</label>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Tempat Lahir *</label>
                                          <div class="col-md-3">
                                              <input type="text"  class="form-control" >
                                          </div>
                                          <label class="col-md-3 control-label" >Tanggal Lahir *</label>
                                          <div class="col-md-3">
                                              <input type="date"  class="form-control dtpicker" style="height: 31px">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Status Pernikahan *</label>
                                          <div class="col-md-9">
                                              <div class="radio">
                                                  <input id="rbtStatus-1" class="magic-radio rbtStatus" type="radio" name="rbtStatus" onClick="setformDataPasangan(this.value)" value="Lajang" checked>
                                                  <label for="rbtStatus-1">Lajang</label>
                                                  <input id="rbtStatus-2" class="magic-radio rbtStatus" type="radio" name="rbtStatus" onClick="setformDataPasangan(this.value)" value="Menikah">
                                                  <label for="rbtStatus-2">Menikah</label>
                                                  <input id="rbtStatus-3" class="magic-radio rbtStatus" type="radio" name="rbtStatus" onClick="setformDataPasangan(this.value)" value="Cerai">
                                                  <label for="rbtStatus-3">Cerai</label>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="clearfix"></div>
                                  <div class="col-sm-10 field-group" id="formDataPasangan">
                                      <h3 class="h6 mar-ver"><span>Informasi Data Pasangan</span></h3>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Nama Istri/ Suami *</label>
                                              <div class="col-md-8">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Alamat *</label>
                                              <div class="col-md-8">
                                                  <input id="chkAlamatSama1" class="magic-checkbox" type="checkbox">
                                                  <label for="chkAlamatSama1">satu rumah dengan pemohon</label>
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >No. Telp/ HP *</label>
                                              <div class="col-md-8">
                                              <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Email *</label>
                                              <div class="col-md-8">
                                                  <input type="text"  class="form-control" >
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-sm-10">
                                          <div class="form-group">
                                              <label class="col-md-4 control-label" >Pekerjaan *</label>
                                              <div class="col-md-5">
                                              <select class="selectpicker form-control">
                                                  <option>Karyawan Swasta</option>
                                                  <option>Wiraswasta</option>
                                                  <option>Ibu Rumah Tangga</option>
                                                  <option>Lainnya</option>
                                              </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="mar-ver"></div>
                                      <div class="clearfix mar-ver"></div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Nama Ibu Kandung *</label>
                                          <div class="col-md-9">
                                              <input type="text"  class="form-control" >
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Alamat sesuai KTP *</label>
                                          <div class="col-md-9">
                                          <input type="text"  class="form-control" id="alamat_ktp">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Alamat Tinggal Sekarang *</label>
                                          <div class="col-md-9">
                                              <input id="chkAlamatSama2" class="magic-checkbox" type="checkbox">
                                              <label for="chkAlamatSama2">sama dengan Alamat KTP</label>
                                              <input type="text"  class="form-control" id="alamatSama">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Nomor Telepon/HP *</label>
                                          <div class="col-md-3">
                                              <input type="text"  class="form-control" >
                                          </div>
                                          <label class="col-md-3 control-label" >Email *</label>
                                          <div class="col-md-3">
                                              <input type="text"  class="form-control" >
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Status Kepemilikan Rumah yang ditempati *</label>
                                          <div class="col-md-5">
                                              <select class="selectpicker form-control">
                                                  <option>Milik Sendiri, tidak dijaminkan</option>
                                                  <option>Milik Sendiri, dijaminkan</option>
                                                  <option>Keluarga</option>
                                                  <option>Sewa</option>
                                                  <option>Dinas</option>
                                              </select>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Lama Menempati *</label>
                                          <div class="col-md-9">
                                              <div class="col-md-2"><input type="number" min="0"  class="form-control" ></div><div class="col-md-3"> tahun</div>
                                              <div class="col-md-2"><input type="number" min="0"  class="form-control" ></div><div class="col-md-3">bulan</div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-sm-10">
                                      <div class="form-group">
                                          <label class="col-md-3 control-label" >Pendidikan Terakhir *</label>
                                          <div class="col-md-5">
                                              <select class="selectpicker form-control">
                                                  <option>SD</option>
                                                  <option>SMP</option>
                                                  <option>SMA</option>
                                                  <option>Diploma</option>
                                                  <option>Strata</option>
                                                  <option>Tidak Tamat SD</option>
                                              </select>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            </div>

                            <div class="col-sm-12">
                                <h3 class="h5 mar-ver"><span>Referensi Kerabat Tidak Serumah</span></h3>
                                <div class="pad-hor">
		                                <div class="col-sm-10">
				                                <div class="form-group">
					                                <label class="col-md-3 control-label" >Nama *</label>
					                                <div class="col-md-9">
							                                <input type="text"  class="form-control" >
					                                </div>
				                                </div>
		                                </div>
		                                <div class="col-sm-10">
				                                <div class="form-group">
					                                <label class="col-md-3 control-label" >Nomor Telepon/HP *</label>
					                                <div class="col-md-9">
					                                		<input type="text"  class="form-control" >
					                                </div>
				                                </div>
		                                </div>
		                                <div class="col-sm-10">
				                                <div class="form-group">
						                                <label class="col-md-3 control-label" >Hubungan *</label>
						                                <div class="col-md-9">
						                                		<input type="text"  class="form-control" >
						                                </div>
				                                </div>
		                                </div>
		                                <div class="col-sm-10">
				                                <div class="form-group">
						                                <label class="col-md-3 control-label" >Alamat *</label>
						                                <div class="col-md-9">
						                                		<input type="text"  class="form-control" >
						                                </div>
				                                </div>
		                                </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
		                            <h3 class="h5 mar-ver"><span>Rekening Bank Aktif</span></h3>
		                            <div class="pad-hor">
				                            <div class="col-sm-4 mar-ver">
				                            		<button class="btn btn-primary btn-hover-mint"><i class="fa fa-plus-circle"></i> Tambah Rekening</button>
				                            </div>
				                            <div class="col-sm-10">
						                            <div class="form-group">
								                            <label class="col-md-3 control-label" >Nama Bank *</label>
								                            <div class="col-md-9">
								                            		<input type="text"  class="form-control" >
								                            </div>
						                            </div>
				                            </div>
				                            <div class="col-sm-10">
						                            <div class="form-group">
						                            		<label class="col-md-3 control-label" >Cabang *</label>
								                            <div class="col-md-9">
								                            		<input type="text"  class="form-control" >
								                            </div>
						                            </div>
				                            </div>
				                            <div class="col-sm-10">
						                            <div class="form-group">
						                            		<label class="col-md-3 control-label" >Nomor Rekening *</label>
								                            <div class="col-md-9">
								                            		<input type="text"  class="form-control" >
								                            </div>
						                            </div>
				                            </div>
				                            <div class="col-sm-10">
						                            <div class="form-group">
								                            <label class="col-md-3 control-label" >Atas Nama *</label>
								                            <div class="col-md-9">
								                            		<input type="text"  class="form-control" >
								                            </div>
						                            </div>
				                            </div>
		                            </div>
                            </div>

                            <div class="col-sm-12 mar-ver">
		                            <div class="col-sm-4">
		                            	<a class="btn btn-default btn-block mar-ver" type="button" href="{{ redirect()->back()->getTargetUrl() }}">Tutup Halaman ini</a>
		                            </div>
		                            <div class="col-sm-4">
		                            	<button class="btn btn-primary btn-block mar-ver" type="submit">Simpan</button>
		                            </div>
                            </div>

                          </form>
                      </div>
                  </div>
              </div>
						@endforeach
            </div>
					</div>
				@include('agent.sidebar')
			</div>
		@endforeach
	</div>
@endsection

@push('script')
		<script>
		$(document).ready(function(){

			$('#formDataPasangan').hide();

			$('#chkAlamatSama2').on('click', function(){
					if ($('#chkAlamatSama2').is(':checked')) {
							var alamat_ktp = $('#alamat_ktp').val();
							//alert(alamat_ktp);
							$('#alamatSama').val(alamat_ktp);
					} else {
							$('#alamatSama').val('');
					}
			});

			$('#rbtStatus').click(function(){
				if ($(this).val()=='Menikah') {
						$('#formDataPasangan').show();
						}else{
						$('#formDataPasangan').hide();
				}
			})
		});

    function setformDataPasangan(val) {
			if (val=='Menikah') {
				$('#formDataPasangan').show();
			}else{
				$('#formDataPasangan').hide();
			}
		}

		</script>
@endpush
