@extends('layouts.app')

@section('content')
	<div id="container" class="effect aside-float aside-bright mainnav-lg">

		@foreach($agents as $agent)

		<div class="boxed">
			@include('agent.navigation')

			<div id="content-container">
				<div id="page-content">
					<div class="panel">
						<div class="panel-body pad-no">
							<h3 class="panel-title">Pengaturan Profil Saya</h3>
							<ol class="breadcrumb">
								<li class="home">Beranda</li>
								<li>Profil Saya</li>
								<li class="active">Pengaturan Profil Saya</li>
							</ol>
						</div>
						<div class="panel-body pad-hor">
							<div class="tab-base">
								<ul class="nav nav-tabs pad-hor">
									<li class="active">
										<a data-toggle="tab" href="#demo-lft-tab-1">Biodata Diri</a>
									</li>
									<li>
										<a data-toggle="tab" href="#demo-lft-tab-2">Daftar Alamat</a>
									</li>
									<li>
										<a data-toggle="tab" href="#demo-lft-tab-3">Rekening Bank</a>
									</li>
									<li>
										<a data-toggle="tab" href="#demo-lft-tab-4">Sertifikat</a>
									</li>
								</ul>
								<div class="tab-content">
									<div id="demo-lft-tab-1" class="tab-pane fade active in">
										<div class="fixed-fluid">
											<div class="fixed-sm-300 fixed-lg-350 pull-sm-left">
												<div class="panel">
													<div class="text-center pad-all bord-btm ">
														<div class="pad-ver">
															<img src="{{ Storage::url('public/agent_profile/' . $agent->agent_image) }}" class="img-lg img-border img-square" alt="Profile Picture">
														</div>
														<div class="text-center">
															<button class="btn btn-sm btn-primary btn-fill" type="submit"><i class="icon-fw"></i> Unggah File</button>
														</div>
														<div class="text-center bord-btm">
															<h4 class="text-lg text-overflow mar-top">{{ $agent->nama }}</h4>
															<p class="text-sm text-muted">Agen FINTAG Edisi MARITIME</p>
															<div class="row pad-all">
																<div class="col-sm-6 ">
																	<p class="h4 mar-no"><i class="demo-psi-star icon-fw text-warning"></i>43.7</p>
																	<small class="text-muted">Point</small>
																</div>
																<div class="col-sm-6">
																	<p class="h4 mar-no">-</p>
																	<small class="text-muted">Rating</small>
																</div>
															</div>
															<div class="text-left pad-btm bord-btm">
																<div class="mar-ver col-sm-6">
																	<h4 class="text-lg text-overflow">Foto KTP</h4>
																</div>
																<div class="mar-top col-sm-6">
																	<button class="btn btn-sm btn-primary pull-right btn-fill" type="submit">
																		<i class="demo-psi-right-4 icon-fw"></i> Unggah File
																	</button>
																</div>

																<div class="pad-ver mar-btm ">

																</div>
																<a href="#"><img class="img-responsive  img-border " src="{{ Storage::url('public/agent_ktp/' . $agent->ktp_image) }}" alt="KTP"></a>

															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="fluid">
												<div class="fixed-fluid">
													<div class="fluid">
														<div class="panel">
															<div class="panel-heading">
																<h3 class="panel-title">Ubah Biodata Diri</h3>
															</div>
															<div class="panel-body">
																<form action="{{ url('agent/profile/' . $agent->id) }}" class="form-horizontal" method="post">
																	{{ csrf_field() }}
																	<div class="panel-body">
																		<input type="hidden" name="id" value="{{ $agent->id }}" id="id">
																		<div class="form-group">
													              <label class="col-sm-3 control-label" for="demo-hor-inputemail">Email</label>
													              <div class="col-sm-9">
													                  <input type="email" placeholder="Email" id="demo-hor-inputemail" class="form-control"  value="{{ Auth::user()->email }}" disabled>
													              </div>
													          </div>
																		<div class="form-group">
													              <label class="col-sm-3 control-label" >Username</label>
													              <div class="col-sm-9">
													                  <input type="text" placeholder="username"  class="form-control" value="{{ Auth::user()->username }}" disabled>
													              </div>
													          </div>
													          <div class="form-group">
													              <label class="col-sm-3 control-label" >Nama</label>
													              <div class="col-sm-9">
													                  <input type="text" placeholder="Nama" value="{{ $agent->nama }}" class="form-control" name="nama" id="nama">
													              </div>
													          </div>
													          <div class="form-group">
													              <label class="col-sm-3 control-label" >No. Telp</label>
													              <div class="col-sm-9">
													                  <input type="text" placeholder="No. Telp" value="{{ $agent->no_telp }}" class="form-control" name="no_telp" id="no_telp">
													              </div>
													          </div>
													          <div class="form-group">
													              <label class="col-sm-3 control-label" >No KTP</label>
													              <div class="col-sm-9">
													                  <input type="text" placeholder="NoKTP" value="{{ $agent->nik }}" class="form-control" name="nik" id="nik">
													              </div>
													          </div>
															</div>
															<div class="panel-footer text-right">
													      	<button class="btn btn-primary btn-fill" type="submit" id="update">Simpan</button>
													    </div>
														</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="demo-lft-tab-2" class="tab-pane fade">
										<div class="fixed-fluid">
											<div class="fluid">
												<div class="panel">
													<div class="panel-body">
														<form action="" class="form-horizontal">
															<div class="panel-body">
																<div class="col-sm-12">
																	<div class="form-group">
																		<h3 class="h5 mar-ver">
																			<span>Alamat sesuai KTP</span>
																		</h3>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control" name="provinsi_ktp">
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label class="control-label">Alamat Lengkap *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<input type="text" class="form-control" placeholder="Alamat Lengkap" name="alamat_ktp" required>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<h3 class="h5 mar-ver">
																			<span>Alamat sesuai KTP</span>
																		</h3>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="{{ $agent->provinsi }}">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-6">
																	<div class="form-group">
																		<label class="control-label">Pilih Provinsi *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<select id="provinsi" class="form-control selectpicker" name="provinsi_ktp"  onChange="genOptKabupaten(this.value)" >
																				<option value="" selected>
																					--Pilih Provinsi--
																				</option>
																				<option value="Jawa Timur">
																					Jawa Timur
																				</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<label class="control-label">Alamat Lengkap *</label>
																		<div class="input-group col-sm-6 col-xs-12">
																			<input type="text" class="form-control" placeholder="Alamat Lengkap" name="alamat_ktp" required>
																		</div>
																	</div>
																</div>
															</div>
															<div class="panel-footer text-right">
											          <button class="btn btn-primary btn-gradient btn-fill" type="submit">
											            	Simpan
											          </button>
											        </div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="demo-lft-tab-3" class="tab-pane fade">
										<div class="panel">
											<div class="panel-body">
												<form action="" class="form-horizontal">
													<div class="panel-body">
														<div class="form-group fix">
									                        <label class="col-sm-3 control-label" >No Rekening</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="No Rekening"  class="form-control">
									                        </div>
									                    </div>

									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Nama Bank</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="Nama Bank"  class="form-control">
									                        </div>
									                    </div>
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Atas Nama</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="Atas Nama"  class="form-control">
									                        </div>
									                    </div>
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Cabang</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="Cabang"  class="form-control">
									                        </div>
									                    </div>
													</div>
													<div class="panel-footer text-right">
									            <button class="btn btn-primary btn-fill" type="submit">
									              Simpan
									            </button>
									        </div>
												</form>
											</div>
										</div>
									</div>

									<div id="demo-lft-tab-4" class="tab-pane fade">
										<div class="panel">
											<div class="panel-body">
												<form class="form-horizontal">
													<div class="panel-body">
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >No Sertifikat</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="No Sertifikat"  class="form-control">
									                        </div>
									                    </div>
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Tgl. Sertifikat</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="Tgl. Sertifikat"  class="form-control dtpicker">
									                        </div>
									                    </div>
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Masa Berlaku</label>
									                        <div class="col-sm-9">
									                            <input type="text" placeholder="Masa Berlaku"  class="form-control dtpicker">
									                        </div>
									                    </div>
									                    <div class="form-group">
									                        <label class="col-sm-3 control-label" >Browse File</label>
									                        <div class="col-sm-9">
																<div class="col-sm-4">
																	<a href="#"><img class="img-responsive img-border " src="{{ asset('img/shared-img.jpg') }}" alt="Sertifikat"></a>
																</div>
									                        </div>
									                    </div>
									                </div>
													<div class="panel-footer text-right">
									                    <button class="btn btn-primary btn-gradient btn-fill" type="submit">Simpan</button>
									                </div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			@include('agent.sidebar')
		</div>

		@endforeach
	</div>

	@if (session('message'))
			@push('script')
					<script>
					$.niftyNoty({
									type: 'info',
									title: 'Message.',
									message: 'Biodata diri berhasil diupdate',
									container: 'floating',
									timer: 5000
								});
					</script>
			@endpush
	@endif

@endsection
