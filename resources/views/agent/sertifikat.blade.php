@extends('layouts.app')

@section('content')

	<div id="container" class="effect aside-float aside-bright mainnav-lg">
		<div id="bg-overlay" class="bg-img img-balloon1"></div>

			<div class="cls-content mar-btm">
				<div class="cls-content-lg panel">
					<div class="panel-heading mar-ver">
						<h3 class="panel-title text-center pad-ver">Lengkapi Data Sertifikat Anda</h3>
					</div>

					<div class="mar-ver pad-top">
            <div class="panel-body">
              <form action="/agent/sertifikat" method="post" enctype="multipart/form-data" id="verifikasiProfile">
  		            {{ csrf_field() }}

                  <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="Nama anda" name="nama" value="{{ old('nama') }}">
                            @if($errors->has('nama'))
                              <small class="help-block">
                                <strong>{{ $errors->first('nama') }}</strong>
                              </small>
                            @endif
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group{{ $errors->has('no_sertifikat') ? ' has-error' : '' }}">
                            <input type="text" class="form-control" placeholder="No Sertifikat Anda" name="no_sertifikat" value="{{ old('no_sertifikat') }}">
                            @if($errors->has('no_sertifikat'))
                              <small class="help-block">
                                <strong>{{ $errors->first('no_sertifikat') }}</strong>
                              </small>
                            @endif
                        </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group{{ $errors->has('tgl_awal') ? ' has-error' : '' }}">
                              <input type="date" class="form-control" placeholder="Tanggal Berlaku" name="tgl_awal" value="{{ old('tgl_awal') }}" style="height: 31px;">
                              @if($errors->has('tgl_awal'))
                                <small class="help-block">
                                  <strong>{{ $errors->first('tgl_awal') }}</strong>
                                </small>
                              @endif
                          </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="form-group{{ $errors->has('tgl_akhir') ? ' has-error' : '' }}">
                              <input type="date" class="form-control" placeholder="Tanggal Berakhir" name="tgl_akhir" value="{{ old('tgl_akhir') }}" style="height: 31px;">
                              @if($errors->has('tgl_akhir'))
                                <small class="help-block">
                                  <strong>{{ $errors->first('tgl_akhir') }}</strong>
                                </small>
                              @endif
                          </div>
                      </div>
                  </div>

                  <div class="row">
  									<div class="col-sm-8">
  									    <span class="pull-left btn btn-sm btn-warning btn-fill btn-file" id="filename">
  									      	Upload Scan Sertifikat Terbaru
  							        	  <input type="file" name="sertifikat_image" id="img_sertifikat">
  							        </span>
  							    </div>
  								</div>

                  <div class="row">
    								<div class="col-sm-12">
    									<button type="submit" class="btn btn-info btn-block btn-fill btn-sm mar-top">Simpan</button>
    								</div>
    							</div>

                  <div class="row">
    								<div class="col-sm-12">
    									<p class="pad-ver mar-no text-center">Belum punya Sertifikat ? <a href="#" class="btn-link text-bold">Daftar</a> </p>
    								</div>
    							</div>
  		        </form>
  					</div>
					</div>
				</div>
			</div>

		</div>
	</div>

@endsection
