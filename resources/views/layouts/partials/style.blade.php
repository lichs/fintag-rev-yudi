  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
  <!--Bootstrap Stylesheet [ REQUIRED ]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <!--Nifty Stylesheet [ REQUIRED ]-->
  <link href="{{ asset('css/nifty.min.css') }}" rel="stylesheet">
  <!--Nifty Premium Icon [ DEMONSTRATION ]-->
  <link href="{{ asset('css/demo/nifty-demo-icons.min.css')}}" rel="stylesheet">
  <!--Demo [ DEMONSTRATION ]-->
  <link href="{{ asset('css/demo/nifty-demo.min.css')}}" rel="stylesheet">
  <!--Magic Checkbox [ OPTIONAL ]-->
  <link href="{{ asset('plugins/magic-check/css/magic-check.min.css')}}" rel="stylesheet">
  <!--Pace - Page Load Progress Par [OPTIONAL]-->
  <link href="{{ asset('plugins/pace/pace.min.css')}}" rel="stylesheet">

  <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

  <!--Bootstrap Table [ OPTIONAL ]-->
  <link href="{{ asset('plugins/bootstrap-table/bootstrap-table.min.css')}}" rel="stylesheet">

  <!-- Fintag Custom theme -->
  <link href="{{ asset('css/custom.css')}}" rel="stylesheet">

  @stack('pushStyle')
