  <!--Pace - Page Load Progress Par [OPTIONAL]-->
  <script src="{{ asset('plugins/pace/pace.min.js')}}"></script>
  <!--jQuery [ REQUIRED ]-->
  <script src="{{ asset('js/jquery-2.2.4.min.js')}}"></script>
  <!--BootstrapJS [ RECOMMENDED ]-->
  <script src="{{ asset('js/bootstrap.min.js')}}"></script>
  <!--NiftyJS [ RECOMMENDED ]-->
  <script src="{{ asset('js/nifty.min.js')}}"></script>
  <!--Demo script [ DEMONSTRATION ]-->
  <script src="{{ asset('js/demo/nifty-demo.min.js')}}"></script>
  <!--Bootstrap Table Sample [ SAMPLE ]-->
  <script src="{{ asset('js/demo/tables-bs-table.js')}}"></script>
  <!--X-editable [ OPTIONAL ]-->
  <script src="{{ asset('plugins/x-editable/js/bootstrap-editable.min.js')}}"></script>
  <!--Bootstrap Table [ OPTIONAL ]-->
  <script src="{{ asset('plugins/bootstrap-table/bootstrap-table.min.js')}}"></script>
  <!--Bootstrap Table Extension [ OPTIONAL ]-->
  <script src="{{ asset('plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.js')}}"></script>

  @stack('script')
